FROM openjdk:13
VOLUME /tmp
ADD ./target/springboot-servicios-productos-0.0.1-SNAPSHOT.jar servicio-producto.jar
ENTRYPOINT ["java","-jar","/servicio-producto.jar"]
